<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Manager\ProductManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RouterController extends AbstractController
{

    #[Route('/products/{id}', name: 'productDetail')]
    public function showPokemon(EntityManagerInterface $doctrine, $id)
    {
        $response = $doctrine->getRepository(Product::class);
        $products = $response->find($id);
        return $this->render('products/productDetail.html.twig', ['product' => $products]);
    }


    #[Route('/products', name: 'productList')]

    public function listProduct(EntityManagerInterface $doctrine)
    {

        $response = $doctrine->getRepository(Product::class);
        $products = $response->findAll();

        return $this->render('products/productList.html.twig', ['products' => $products]);
    }


    #[Route('/new/product', name: 'newProduct')]
    public function newProduct(Request $request, EntityManagerInterface $doctrine,  ProductManager $manager){

        $form= $this-> createForm(ProductType::class);
        $form->handleRequest($request);
        if($form-> isSubmitted() && $form-> isValid()){
            $product = $form-> getData();
            $fileImage = $form -> get('Imagen') -> getData();
            if($fileImage){

                $image = $manager -> upload($fileImage, $this->getParameter('kernel.project_dir').'/public/asset/image');
                $product-> setImage("/asset/image/".$image);
            }
            $doctrine->persist($product);
            $doctrine->flush();
            $this-> addFlash('success', 'Producto agregado correctamente');
            return $this-> redirectToRoute('productList');

        }

        return $this-> renderForm('products/newProductForm.html.twig', [

            'form'=> $form
        ]);

    }


    #[Route('/edit/product/{id}', name: 'editProduct')]
    public function editProduct(Request $request, EntityManagerInterface $doctrine, $id){

        $response = $doctrine->getRepository(Product::class);
        $products = $response->find($id);

        $form= $this-> createForm(ProductType::class, $products);
        $form->handleRequest($request);
        if($form-> isSubmitted() && $form-> isValid()){
            $product = $form-> getData();
            $doctrine->persist($product);
            $doctrine->flush();
            $this-> addFlash('success', 'Se ha actualizado la lista');
            return $this-> redirectToRoute('productList');

        }

        return $this-> renderForm('products/newProductForm.html.twig', [

            'form'=> $form
        ]);

    }

    #[Route('/delete/product/{id}', name: 'deleteProduct')]
    public function deleteProduct(EntityManagerInterface $doctrine, $id)
    {
        $response = $doctrine->getRepository(Product::class);
        $products = $response->find($id);
        $doctrine->remove($products);
        $doctrine->flush();
        $this-> addFlash('success', 'El producto ha sido eliminado');
        return $this-> redirectToRoute('productList');
    }
}

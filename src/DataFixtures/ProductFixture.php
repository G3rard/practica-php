<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ProductFixture extends Fixture
{
    protected $client;
    public function __construct(HttpClientInterface $client)
    {
       $this -> client=$client;     
    }


    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        for ($i=0; $i<50; $i++){
            $numItem = $faker -> numberBetween(1,20);
            $response = $this->client->request(
                'GET',
                "https://fakestoreapi.com/products/$numItem"
            );
            $contenido = $response->toArray();
            $product = new Product();
            $product->setTitle($contenido['title']);
            $product->setPrice($contenido['price']);
            $product->setDescription($contenido['description']);
            $product->setImage($contenido['image']);
            $product->setCategory($contenido['category']);
            $manager->persist($product);
           }
    

        $manager->flush();
    }
}

<?php

namespace App\Manager;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductManager
{

    public function upload(UploadedFile $file, $folder){

        $fileName = uniqid().'.'.$file->guessClientExtension();
        $file-> move($folder, $fileName);
        return $fileName;

    }

}

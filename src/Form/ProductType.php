<?php

namespace App\Form;

use App\Entity\Product;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TypeTextType::class, [
                'label' => 'Nombre del producto',
                'attr' => ['placeholder' => 'Ej. Zapatos']
            ])
            ->add('price', TypeTextType::class,[
                'label' => 'Precio',
                'attr' => ['placeholder' => 'Precio del producto']
            ])
            ->add('description', TypeTextType::class,[
                'label' => 'Descripción',
            ])
            ->add('Imagen', FileType::class, ['mapped' => false])
            ->add('enviar', SubmitType::class)
            // ->add('category')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
